/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import DAO.ShapesDAO;
import Models.Circle;
import Models.Elipse;
import Models.Line;
import Models.Rectangle;
import Models.Square;
import java.awt.Graphics;

/**
 *
 * @author Usuario
 */
public class Helper {
    Graphics graphics;
    
    public Helper (Graphics graphics){
        this.graphics = graphics;
        
    }
    
    public void draw(ShapesDAO shapes){
        for(Rectangle rect : shapes.getRectangle()){
            graphics.setColor(rect.getColor());
            
            if(rect.getWidth()< 0 && rect.getHeight() > 0)
                graphics.fillRect(rect.getX()+rect.getWidth(), rect.getY(), rect.getWidth()*-1, rect.getHeight());
            
            else if(rect.getWidth()> 0 && rect.getHeight() < 0)
                graphics.fillRect(rect.getX(), rect.getY()+rect.getHeight(), rect.getWidth(), rect.getHeight()*-1);
            
            else if(rect.getWidth()< 0 && rect.getHeight() < 0)
                graphics.fillRect(rect.getX()+rect.getWidth(), rect.getY()+rect.getHeight(), rect.getWidth()*-1, rect.getHeight()*-1);
            
            else
                graphics.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        }
            
        for(Square sq : shapes.getSquare()){
            graphics.setColor(sq.getColor());
            
            if(sq.getWidth()< 0 && sq.getHeight() > 0)
                graphics.fillRect(sq.getX()+sq.getWidth(), sq.getY(), sq.getWidth()*-1, sq.getHeight());
            
            else if(sq.getWidth()> 0 && sq.getHeight() < 0)
                graphics.fillRect(sq.getX(), sq.getY()+sq.getHeight(), sq.getWidth(), sq.getHeight()*-1);
            
            else if(sq.getWidth()< 0 && sq.getHeight() < 0)
                graphics.fillRect(sq.getX()+sq.getWidth(), sq.getY()+sq.getHeight(), sq.getWidth()*-1, sq.getHeight()*-1);
            
            else
                graphics.fillRect(sq.getX(), sq.getY(), sq.getWidth(), sq.getHeight());
        }
        
        for(Elipse el : shapes.getElipse()){
            graphics.setColor(el.getColor());
            if(el.getWidth()< 0 && el.getHeight() > 0)
                graphics.fillOval(el.getX()+el.getWidth(), el.getY(), el.getWidth()*-1, el.getHeight());
                
            else if(el.getWidth()> 0 && el.getHeight() < 0)
                graphics.fillOval(el.getX(), el.getY()+el.getHeight(), el.getWidth(), el.getHeight()*-1);
            
            else if(el.getWidth()< 0 && el.getHeight() < 0)
                graphics.fillOval(el.getX()+el.getWidth(), el.getY()+el.getHeight(), el.getWidth()*-1, el.getHeight()*-1);
            
            else
                graphics.fillOval(el.getX(), el.getY(), el.getWidth(), el.getHeight());
        }
        
        for(Line li : shapes.getLines()){
            graphics.setColor(li.getColor());
            graphics.drawLine(li.getOrigenX(), li.getOrigenY(), li.getDestinoX(), li.getDestinoY());
                       
        }
        
        for(Circle cir : shapes.getCircles()){
            graphics.setColor(cir.getColor());
            if(cir.getWidth()< 0 && cir.getHeight() > 0)
                graphics.fillOval(cir.getX()+cir.getWidth(), cir.getY(), cir.getWidth()*-1, cir.getHeight());
                
            else if(cir.getWidth()> 0 && cir.getHeight() < 0)
                graphics.fillOval(cir.getX(), cir.getY()+cir.getHeight(), cir.getWidth(), cir.getHeight()*-1);
            
            else if(cir.getWidth()< 0 && cir.getHeight() < 0)
                graphics.fillOval(cir.getX()+cir.getWidth(), cir.getY()+cir.getHeight(), cir.getWidth()*-1, cir.getHeight()*-1);
            
            else
                graphics.fillOval(cir.getX(), cir.getY(), cir.getWidth(), cir.getHeight());
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAO.ShapesDAO;
import Models.Circle;
import Models.Elipse;
import Models.Line;
import Models.Rectangle;
import Models.Square;
import Views.MainWindow;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Usuario
 */
public class MarkerController {
    MainWindow window;
    ShapesDAO shapes;
    Gson json = new GsonBuilder().setPrettyPrinting().create();
    int x,y,x2,y2;
    Rectangle rectangle;
    Square square;
    Elipse elipse;
    Line line;
    Circle circle;
    String serialized;
    String pathImage;
    String pathSave;
    String pathRecover;
    Color colorSelect = Color.BLACK;
    
    
    // TODO code application logic here
    public MarkerController() {
        
        shapes = new ShapesDAO();
        window = new MainWindow(shapes);
        initComponents();
    }
    
    public final void initComponents(){
        
        window.setPanel();
        window.setVisible(true);
        eventSelectElipse();
        eventSelectRectangle();
        eventSelectSquare();
        eventSelectLine();
        eventSelectCircle();
        eventRecuperar ();
        eventGuardar ();
        eventCargar();
        cargarImage();
        eventExit();
        draw();
    }
    
    public void draw()
    {
        MouseListener listener = new MouseListener() 
        {
            @Override
            public void mouseClicked(MouseEvent e) {
                
            }

            @Override
            public void mousePressed(MouseEvent e) {
                x= e.getX();
                y= e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                x2= e.getX();
                y2= e.getY();
                if("Rectangle".equals(window.getShapeSelected())){
                    rectangle = new Rectangle (x,y,x2,y2,window.getColorSelected());
                    shapes.addShape(rectangle);
                }
                if("Square".equals(window.getShapeSelected())){
                    square = new Square (x,y,x2,y2,window.getColorSelected());
                    shapes.addShape(square);
                }
                if("Elipse".equals(window.getShapeSelected())){
                    elipse = new Elipse (x,y,x2,y2,window.getColorSelected());
                    shapes.addShape(elipse);
                }
                if("Line".equals(window.getShapeSelected())){
                    line = new Line (x,y,x2,y2,window.getColorSelected());
                    shapes.addShape(line);
                }
                if("Circle".equals(window.getShapeSelected())){
                    circle = new Circle (x,y,x2,y2,window.getColorSelected());
                    shapes.addShape(circle);
                }
                window.getPanel().repaint();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                
            }
            
        };
        window.getPanel().addMouseListener(listener);
    }
    
    public void eventSelectElipse () {
        ActionListener action = (ActionEvent e) -> {
            colorSelect = JColorChooser.showDialog(null,
            "Seleccione el color", Color.BLACK);
            window.setColorSelected(colorSelect);
            window.setShapeSelected("Elipse");
        };
        window.getElipse().addActionListener(action);
    }
    
    public void eventSelectSquare () {

        ActionListener action = (ActionEvent e) -> {
            colorSelect = JColorChooser.showDialog(null,
            "Seleccione el color", Color.BLACK);
            window.setColorSelected(colorSelect);
            window.setShapeSelected("Square");
        };
        window.getSquare().addActionListener(action);
    }
    
    public void eventSelectRectangle () {
        ActionListener action = (ActionEvent e) -> {
            colorSelect = JColorChooser.showDialog(null,
            "Seleccione el color", colorSelect);
            window.setColorSelected(colorSelect);
            window.setShapeSelected("Rectangle");
        };
        window.getRectangle().addActionListener(action);
    }
    
    public void eventSelectCircle () {
        ActionListener action = (ActionEvent e) -> {
            colorSelect = JColorChooser.showDialog(null,
            "Seleccione el color", colorSelect);
            window.setColorSelected(colorSelect);
            window.setShapeSelected("Circle");
        };
        window.getCircle().addActionListener(action);
    }
    
    public void eventSelectLine () {
        ActionListener action = (ActionEvent e) -> {
            colorSelect = JColorChooser.showDialog(null,
            "Seleccione el color", colorSelect);
            window.setColorSelected(colorSelect);
            window.setShapeSelected("Line");
        };
        window.getLine().addActionListener(action);
    }
    
    
    public void eventGuardar (){
        window.getGuardar().addActionListener((java.awt.event.ActionEvent evt) -> {
            
            JFileChooser file = new JFileChooser ();
            file.setCurrentDirectory(new File(System.getProperty("user.home")));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.json","json");
            file.addChoosableFileFilter(filter);
            int result = file.showSaveDialog(null);
            if(result == JFileChooser.APPROVE_OPTION){
                File selectedFile = file.getSelectedFile();
                pathSave = selectedFile.getAbsolutePath()+".json";

                try {                                    
                    Gson json1 = new GsonBuilder().setPrettyPrinting().create();
                    try (FileWriter writer = new FileWriter(pathSave)) {
                        json1.toJson(shapes, writer);
                        writer.flush();
                    }
                }catch (IOException ex) {
                    Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    public void eventRecuperar (){
        window.getRecuperar().addActionListener((java.awt.event.ActionEvent evt) -> {
            try {
                
                JFileChooser file = new JFileChooser ();
                file.setCurrentDirectory(new File(System.getProperty("user.home")));
                FileNameExtensionFilter filter = new FileNameExtensionFilter("*.json","json");
                file.addChoosableFileFilter(filter);
                int result = file.showOpenDialog(null);
                if(result == JFileChooser.APPROVE_OPTION){
                    File selectedFile = file.getSelectedFile();
                    pathRecover = selectedFile.getAbsolutePath();
                    Gson json1 = new GsonBuilder().setPrettyPrinting().create();
                    JsonReader reader = new JsonReader(new FileReader(pathRecover));
                    shapes = json1.fromJson(reader, shapes.getClass());
                    window.setShapes(shapes);
                    initComponents();
                }
            }catch (IOException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    public void eventCargar (){
        window.getCargar().addActionListener((java.awt.event.ActionEvent evt) -> {
            
            JFileChooser file = new JFileChooser ();
            file.setCurrentDirectory(new File(System.getProperty("user.home")));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.images","jpg","gif","png");
            file.addChoosableFileFilter(filter);
            int result = file.showOpenDialog(null);
            if(result == JFileChooser.APPROVE_OPTION){
                File selectedFile = file.getSelectedFile();
                pathImage = selectedFile.getAbsolutePath();
                cargarImage ();
            }
            
        });
    }
    
    public void cargarImage () {
        if(pathImage != null)
            window.setBackground(pathImage);
        
    }
    public void eventExit (){
        window.getExit().addActionListener((java.awt.event.ActionEvent evt) -> {
                window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
            
        });
    }
    
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.awt.Color;

/**
 *
 * @author Usuario
 */

public class Line extends Shape{
    
    int origenX, origenY, destinoX, destinoY;
 
    public Line (int x, int y, int x2, int y2, Color color) {
        super(x, y, color);
        this.origenX = x;
        this.origenY = y;      
        this.destinoX = x2;
        this.destinoY = y2;  
                     
    }

    public int getOrigenX() {
        return origenX;
    }
    
    public int getOrigenY() {
        return origenY;
    }
    
     public int getDestinoX() {
        return destinoX;
    }
    
    public int getDestinoY() {
        return destinoY;
    }

}

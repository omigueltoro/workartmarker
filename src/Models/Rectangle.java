/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.awt.Color;

/**
 *
 * @author Usuario
 */
public class Rectangle extends Shape{
    
    int width, height;

    public Rectangle(int x, int y, int x2, int y2, Color color) {
        super(x, y, color);
        this.width = x2-x;
        this.height = y2-y;
        
    }

    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.awt.Color;

/**
 *
 * @author Usuario
 */
public class Circle extends Shape {
        
    int width, height;
    
    public Circle(int x, int y, int x2, int y2, Color color) {
        super(x, y, color);
        this.width = (x2-x);
        this.height = (x2-x)*( y2-y >= 0 && x2-x >= 0 ? 1 : y2-y <= 0 && x2-x <= 0 ? 1 : y2-y >= 0 && x2-x <= 0 ? -1 : y2-y <= 0 ? -1 : 1);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
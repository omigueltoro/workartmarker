/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.awt.Color;

/**
 *
 * @author Usuario
 */
public class Elipse extends Shape{

    int width, height;
    
    public Elipse(int x, int y, Color color) {
        super(x, y, color);
    }

    public Elipse(int x, int y, int x2, int y2, Color color) {
        super(x, y, color);
        this.width = x2-x;
        this.height = y2-y;
        
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int x,int x2) {
        this.width = x2-x;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int y,int y2) {
        this.height = y2-y;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Circle;
import Models.Elipse;
import Models.Line;
import Models.Rectangle;
import Models.Square;
import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class ShapesDAO {
    ArrayList<Rectangle> rectangles;
    ArrayList<Square> squares;
    ArrayList<Elipse> elipses;
    ArrayList<Circle> circles;
    ArrayList<Line> lines;

    public ShapesDAO() {
        rectangles = new ArrayList();
        squares = new ArrayList();
        elipses = new ArrayList();
        lines = new ArrayList();
        circles = new ArrayList();
    }
    public void addShape(Rectangle r){
        rectangles.add(r);
    }
    public void addShape(Square s){
        squares.add(s);
    }
    public void addShape(Elipse e){
        elipses.add(e);
    }
    public void addShape(Line l){
        lines.add(l);
    }
    public void addShape(Circle c){
        circles.add(c);
    }
    public ArrayList<Rectangle> getRectangle() {
        return rectangles;
    }
    public ArrayList<Square> getSquare() {
        return squares;
    }
    public ArrayList<Elipse> getElipse() {
        return elipses;
    }
    public ArrayList<Line> getLines() {
        return lines;
    }
    public ArrayList<Circle> getCircles() {
        return circles;
    }
}
